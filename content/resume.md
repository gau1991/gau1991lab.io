+++
title = "About Me"
date = "2019-01-31"
menu = "main"
+++

## Professional

### Cloud Architect

at [REAN Cloud](https://reancloud.com/)

Jan 2018 – Jan 2019 | Pune Area, India

* Leading Team and Internal Products
* Participating code reviews and design discussion
* Demos to internal team
* Designing and Setting up AWS infrastructure
* Developing Serverless Applications using AWS Lambda

### Senior Cloud Automation Engineer

at [REAN Cloud](https://reancloud.com/)

Sept 2016 – Dec 2017 | Pune Area, India

* Designing and Setting up AWS infrastructure
* Automation of infrastructure using Terraform and Ansible
* Automation of day to day tasks using Chef
* Setting up complete CI/CD Pipelines
* Setting up scalable infrastructure using Docker
* Build Debian and RPM packages using Omnibus
* Conducting various Workshops on Chef, Ansible

### Cloud Automation Engineer and Python Developer

at [Opex Softwares](http://opexsoftware.com/) ( acquired by [REAN Cloud](https://reancloud.com/) )

April 2016 – August 2016 | Pune Area, India

* Automation of day to day tasks Chef and Ansible
* Automation of infrastructure using Terraform and Packer
* Conducting Chef, Ansible Trainings
* Setup and Deploy Jenkins CI
* Designing and Setting up AWS infrastructure

### Linux System Administrator

at [rtCamp](https://rtcamp.com)

May 2014 – March 2016 | Pune Area, India

* Working as Linux (Ubuntu, Debian) Server/System Administrator.
* Working in Linux Server/System installation, configuration, administration, troubleshooting, monitoring and backup & recovery methodologies.
* Working with various server softwares such as Nginx, PHP, MySQL, GIT
* Setup and Deploy various build tools such as Jenkins and Travis-CI
* Develop and Maintain EasyEngine to automate WordPress setup in different types on Debian/Ubuntu based Linux Servers in Python and Shell Script.
* Build Nginx and PHP packages for Debian and Ubuntu
* Configure and setup various CMS such as WordPress, Discourse
* Configure and Troubleshoot GIT and GitLab

### System Administrator Engineer

at [TCS](http://tcs.com/)

July 2013 – May 2014 | Pune Area, India

* Real time server administration, monitoring, backup and recovery
* Automate day to day task using Shell Script
* Develop, configure and deploy various PHP based applications

## Certifications

### AWS Certified Solution Architect - Associate

* Certificate Validation Number: E55KXZQC1J411LGP
* Certificate Expiry Date: July 01, 2019

## Projects Contributed

### [EasyEngine](https://easyengine.io)

* EasyEngine (ee) is a command line control panel to setup NGINX server on Debian/Ubuntu Linux distribution for HTML, PHP, MySQL, HHVM, PageSpeed and WordPress websites.
* Complete Setup - Install NGINX, PHP, MySQL, Mail Server and dependencies in a single command.
* HHVM & PageSpeed Support - Boost WordPress sites by enabling HHVM and PageSpeed using a single command.
* Caching Options - Use W3Total Cache, WP Super Cache, Nginx FastCGI Cache and Redis Cache.
* Config Optimization - Automatically tweaks server configuration as per available hardware resources.
* Git-Backed Changes - All config changes are saved using Git so feel free to play with config!

### [DoudouLinux](http://www.doudoulinux.org)

* DoudouLinux is specially designed for children to make computer use as easy and pleasant as possible for them
* DoudouLinux provides tens of applications that suit children from 2 to 12 years old and gives them an environment as easy to use as a gaming console.

### Pragat Cluster Automation Toolkit

* Python and PHP based Automation Toolkit to Mass deploy Cent OS based OpenMPI clusters.
* Pragat cluster automation toolkit is part of DRDO sub project

## Education

### Bachelor of Technologies in Information Technology

at [Walchand College Of Engineering](http://www.walchandsangli.ac.in/)

May 2009 – June 2013 | Sangli Area, India

## Skills

### Cloud Services

Amazon Web Services, Google Cloud, Linode, Digital Ocean

### Automation Tools

Chef, Ansible, Terraform, Packer

### Virtualization Platforms

Vagrant, Docker

### Cluster Management

Kubernetes, Docker Swarm

### Continuous Integration tools

Jenkins, GitLab CI, Travis

### Version Control Platforms

Git,  SVN

### Content Management Systems

WordPress, GitLab, Discourse

### Server Management

Nginx, PHP, HHVM, MariaDB

### Logging and Monitoring

ELK, Datadog, CheckMK, Monit

### Languages

Python, Shell Script, PHP, CSS, HTML, C/C++

### Build Tools

Chef Omnibus

### System Administrator

Ubuntu, Debian

## Honor and Awards

* Employee Of the Year 2017 at REAN Cloud - Jan 2018
* Employee of the Month at REAN Cloud – Sept 2017
* Employee Of the Month at TCS - April 2014
* ILP Kudos during TCS Training - Sep 2013
* Student Of the Year in Dept. Of Information Technology - May 2013
